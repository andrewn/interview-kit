# Technical Interview, Manager Role

## Introduction

You will undergo a 45 minute technical interview with myself, Andrew Newdigate. My README is at https://about.gitlab.com/handbook/engineering/infrastructure/team/dei.  

## Conducting the Interview

Here are some pointers to help guide you through the interview:

1. We are conducting this interview to give you a chance to highlight the experience that you have had in your career, and whether it would be a good fit for this specific role. 
45 minutes is not a long time to cover all the technical questions, so please forgive me if I ask you to finish up an answer so that we can move on. I am doing this so that we can highlight more of your experience. 
1. If you don’t understand a question, it’s totally fine to ask me to state it again or to go into more detail into what I’m asking.
1. If I ask a question that you would rather not answer - whether because you don’t feel it applies to your personal experience, you don’t know the answer or any other reasons, please let me know and we’ll move forward onto other questions. I will not count this against you. 
1. Always try to give concrete examples in your answers. Try to avoid “we did X”, rather, be specific and clear about what your role in the project was with “I did X”.
1. Near the end of the interview, I’ll ask you for a question. This is part of the interview and I’ll be assessing your question as a way of understanding how much you took in and what you perceive to be the technical and organisational challenges we face -- please take time to consider what you want to ask. 
1. If you see or hear me typing, please be assured that I’m giving you my full attention - I am just taking notes, not multitasking.  

## Technical areas that I may ask about

1. Building and scaling a terraform environment. Managing state. 
1. Configuration management and provisioning tools such as Ansible, Chef, etc
1. Running workloads in Kubernetes
1. Monitoring, particularly with SLOs, saturation monitoring, capacity planning, alerting
1. Logging and events
1. Debugging production incidents: digging into a high-severity production incident, how you used the tools at your disposal to understand the causes of a problem, how your mitigated the problem, and how you push systems in place to ensure that the problem did not return. 
1. Automation. The tools you use to automate your workflow, how you reduce risk in automation, how you verify that it’s working correctly, etc. 
1. Collecting and analysing data on the performance of your team.
